﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Configuration;
using RabbitMQ.Client;
using System.Text;

namespace RestEndPoints.Models
{
    public class Usuario
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string Password { set; get; }
        public DateTime Create_date { set; get; }
        public DateTime Update_date { set; get; }        
      
    }

    public class UsuarioDTO : Usuario
    {       
        public UsuarioDTO() { }

        public void Create(string name, string pass)
        {
            Usuario user = new Usuario();
            user.Name = name;
            user.Password = pass;
            user.Create_date = DateTime.Now;
            List<Usuario> lista = this.GetDatosFromFile();           
            user.ID = Math.Abs(Guid.NewGuid().GetHashCode());
            lista.Add(user);
            this.SaveDataToFile(lista);
        }

        public List<Usuario> Get()
        {
            List<Usuario> lista = GetDatosFromFile();          
            return lista;
        }

        public Usuario Get(int id)
        {
            List<Usuario> lista = GetDatosFromFile();
            if(lista.Count > 0)
            {
                var elemento = lista.Where(x => x.ID == id).ToList();
                if(elemento.Count == 0)
                {
                    return null;
                }
                else
                {
                    return elemento[0];
                }
            }
            else
            {
                return null;
            }
        }

        public void Delete(int id)
        {
            List<Usuario> lista = GetDatosFromFile();
            lista.RemoveAll(x => x.ID == id);
            this.SaveDataToFile(lista);
        }

        public void Edit(string name, string pass, int id)
        {
            List<Usuario> lista = GetDatosFromFile();
            Usuario user = lista.Where(x => x.ID == id).FirstOrDefault<Usuario>();
            if(user == null)
            {
                throw new Exception("El elemento con id: " + id + " no existe");
            }
            else
            {
                user.Name = name;
                user.Password = pass;
                user.Update_date = DateTime.Now;
                this.Delete(id);
                lista = GetDatosFromFile();
                lista.Add(user);
                this.SaveDataToFile(lista);
            }
        }


        public List<Usuario> GetDatosFromFile()
        {
            try
            {
                string path = WebConfigurationManager.AppSettings["path"];
                string json = string.Empty;
                List<Usuario> lista;
                if (File.Exists(path))
                {
                    json = File.ReadAllText(path);
                    lista = JsonConvert.DeserializeObject<List<Usuario>>(json);
                    if (lista == null)
                    {
                        lista = new List<Usuario>();                       
                    }
                    return lista;
                }
                else
                {
                    StreamWriter sw = new StreamWriter(path);
                    lista = new List<Usuario>();
                    sw.Write(JsonConvert.SerializeObject(lista, Formatting.Indented));
                    sw.Close();
                    this.SendDataToRabbit(lista);
                    return lista;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SaveDataToFile(List<Usuario> lista)
        {
            try
            {
                string path = WebConfigurationManager.AppSettings["path"];
                StreamWriter sw = new StreamWriter(path);                
                sw.Write(JsonConvert.SerializeObject(lista, Formatting.Indented));
                sw.Close();
                this.SendDataToRabbit(lista);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SendDataToRabbit(List<Usuario> listado)
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "sincronizador",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    //string message = "Hello World!";
                    //var body = Encoding.UTF8.GetBytes(message);
                    var datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(listado, Formatting.Indented));

                    channel.BasicPublish(exchange: "",
                                         routingKey: "sincronizador",
                                         basicProperties: null,
                                         body: datos);
                    Console.WriteLine(" [x] Datos Enviados");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
