﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestEndPoints.Models;
using System.Web.Http.Cors;


namespace RestEndPoints.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsuariosController : ApiController
    {
        UsuarioDTO dto = new UsuarioDTO();
        // GET: api/Usuarios
        public IEnumerable<Usuario> Get()
        {
            return dto.Get();
        }

        // GET: api/Usuarios/5
        public Usuario Get(int id)
        {            
            return dto.Get(id);            
        }

        // POST: api/Usuarios      
        [HttpPost]
        public void Post(string pass, string name)
        {
            dto.Create(name, pass);            
        }

        // PUT: api/Usuarios/5
        [HttpPut]
        public void Put(int id,string name, string pass)
        {
            dto.Edit(name, pass, id);
        }

        // DELETE: api/Usuarios/5
        public void Delete(int id)
        {
            dto.Delete(id);
        }

        [HttpOptions]
        public string Options()
        {
            return null;
        }
    }
}
